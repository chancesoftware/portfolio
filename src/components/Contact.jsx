import React from 'react'

const Contact = () => {
  return (
    <div name='contact' className='w-full h-screen bg-[#0a192f] text-gray-300'>
      <div className='flex flex-col justify-center items-center w-full h-full'>
        <div className='max-w-[1000px] w-full grid grid-cols-1 gap-8'>
          <div className='sm:text-left pb-8 pl-4'>
            <p className='text-left text-4xl font-bold inline border-b-4 border-pink-600'>
              Contact
            </p>
            
          </div>
          </div>
          <div className='max-w-[1000px] w-full grid sm:grid-cols-1 gap-8 px-4'>
            <div className='sm:text-left text-4xl'>
              <p>You can reach me at <strong>howe.c.l@gmail.com</strong> with any inquiries.</p>
            </div>
          </div>
      </div>
    </div>
  )
}

export default Contact