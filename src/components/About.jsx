import React from 'react';

const About = () => {
  return (
    <div name='about' className='w-full h-screen bg-[#0a192f] text-gray-300'>
      <div className='flex flex-col justify-center items-center w-full h-full'>
        <div className='max-w-[1000px] w-full grid grid-cols-2 gap-8'>
          <div className='sm:text-left pb-8 pl-4'>
            <p className='text-4xl text-left font-bold inline border-b-4 border-pink-600'>
              About
            </p>
          </div>
          <div></div>
          </div>
          <div className='max-w-[1000px] w-full grid gap-8 px-4'>
            <div className='sm:text-left text-1xl'>
              <p>I have spent the majority of my professional career as a Retail Manager in multiple industries. However, 
                  my passion for Software Development was too strong for me to continue following that career path. Thanks 
                  to a scholarship opportunity provided by the Matrix Code program, I was able to expand on my basic programming 
                  knowledge and seize an employment opportunity as a Junior Software Developer. I never imagined I could have 
                  so much fun overcoming the challenges I face every day.
              </p>
            </div>
          </div>
      </div>
    </div>
  );
};

export default About;