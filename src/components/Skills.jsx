import React from 'react';

import { IoLogoJavascript, 
    IoLogoPython, 
    IoLogoHtml5, 
    IoLogoCss3,
    IoLogoReact
} from "react-icons/io5";
import { SiAmazonaws, 
    SiGitlab, 
    SiPostgresql 
} from "react-icons/si";

const Skills = () => {
  return (
    <div name='skills' className='w-full h-screen bg-[#0a192f] text-gray-300'>
      {/* Container */}
      <div className='max-w-[1000px] mx-auto p-4 flex flex-col justify-center w-full h-full'>
          <div>
              <p className='text-4xl font-bold inline border-b-4 border-pink-600'>Technical Skills</p>
              <p className='py-4'>See a list of Software Technologies I've worked with</p>
          </div>

          <div className='w-full grid grid-cols-2 sm:grid-cols-4 gap-4 text-center py-8'>
              <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-500'>
                  <IoLogoHtml5 className='w-20 mx-auto' size={100} />
                  <p className='my-4'>HTML</p>
              </div>
              <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-500'>
                  <IoLogoCss3 className='w-20 mx-auto' size={100} />
                  <p className='my-4'>CSS</p>
              </div>
              <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-500'>
                  <IoLogoJavascript className='w-20 mx-auto' size={100} />
                  <p className='my-4'>JAVASCRIPT</p>
              </div>
              <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-500'>
                  <IoLogoReact className='w-20 mx-auto' size={100} />
                  <p className='my-4'>REACT</p>
              </div>
              <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-500'>
                  <IoLogoPython className='w-20 mx-auto' size={100} />
                  <p className='my-4'>PYTHON</p>
              </div>
              <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-500'>
                  <SiGitlab className='w-20 mx-auto' size={100} />
                  <p className='my-4'>GITLAB</p>
              </div>
              <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-500'>
                  <SiPostgresql className='w-20 mx-auto' size={100} />
                  <p className='my-4'>POSTGRES</p>
              </div>
              <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-500'>
                  <SiAmazonaws className='w-20 mx-auto' size={100} />
                  <p className='my-4'>AWS</p>
              </div>
          </div>
      </div>
    </div>
  );
};

export default Skills;